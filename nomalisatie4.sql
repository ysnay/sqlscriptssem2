use modernways;

select Boeken.Voornaam, Boeken.Familienaam, Boeken.Personen_Id, Personen.Voornaam, Personen.Familienaam, Personen.Id
from Boeken cross join Personen
where Boeken.Voornaam = Personen.Voornaam and Boeken.Familienaam = Personen.Familienaam;

set sql_safe_updates = 0;

update Boeken cross join Personen
	set Boeken.Personen_Id = Personen.Id
    where Boeken.Voornaam = Personen.Voornaam and Boeken.Familienaam = Personen.Familienaam;
    
set sql_safe_updates = 1;