USE Modernways;
CREATE VIEW GemiddeldeRatings AS
SELECT reviews.Boeken_Id,AVG(Reviews.Rating) AS Rating
FROM Reviews GROUP BY reviews.Boeken_Id;