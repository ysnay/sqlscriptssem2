USE ModernWays;
-- creer een nieuwe tabel met de auteursgegevens
-- voornaam en familienaam

set foreign_key_checks = 0;

drop table if exists Personen;

set foreign_key_checks = 1;

create table Personen(
Voornaam varchar(255) not null,
Familienaam varchar(255) not null
);

-- gegevens uit de tabel boeken overztten naar de tabel Personen
-- we gebruiken hiervoor een subquery
Insert into Personen (Voornaam,Familienaam)
select distinct Voornaam,Familienaam from Boeken;

select Voornaam, Familienaam from Personen
order by Voornaam, Familienaam;

-- overige kolommen aan de tabel Personen toevoegen
-- voegen extra kolommen met primary key

alter table Personen add(
Id int auto_increment primary key,
Aanspreektitel varchar(30) null,
Straat varchar(80) null,
Huisnummer varchar(5) null,
Stad varchar(50) null,
Commentaar varchar(100) null,
Biografie varchar(400) null
);

-- testen wat we hebben
select * from Personen order by Familienaam, Voornaam;

-- de tabel Boeken en Personen linken
-- constraint laten allen, foreign key invullen en dan pas de not null constraint toeveogen
Alter table Boeken add Personen_Id int null;

-- De foreign key kolom in de tabel Boeken invullen

-- realtie tussen boeken en personen
select Boeken.Voornaam, Boeken.Familienaam, Boeken.Personen_Id, Personen.Voornaam, Personen.Familienaam, Personen.Id
from Boeken cross join Personen
where Boeken.Voornaam = Personen.Voornaam and Boeken.Familienaam = Personen.Familienaam;

-- zet safe updates uit
set sql_safe_updates = 0;

update Boeken cross join Personen
	set Boeken.Personen_Id = Personen.Id
    where Boeken.Voornaam = Personen.Voornaam and Boeken.Familienaam = Personen.Familienaam;
    
-- zet safe updates aan
set sql_safe_updates = 1;

-- foreign key verplicht maken
alter table Boeken change Personen_Id Personen_Id int not null;

-- testen 
select Voornaam, Familienaam, Personen_Id from Boeken;

-- Dubbele kolommen verwijderen uit de tabel Boeken;

alter table Boeken drop column Voornaam, drop column Familienaam;

alter table Boeken add constraint fk_Boeken_Personen
foreign key(Personen_Id) references Personen(Id);



