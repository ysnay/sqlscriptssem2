use aptunes;

delimiter $$
create procedure GetAlbumDuration2(IN album int, OUT totalDuration smallint unsigned)
sql security invoker
begin
	declare songDuration tinyint unsigned default 0;
    declare ok bool default false;
	declare songDurationCursor cursor for select Lengte from Liedjes where Albums_Id = album;
    declare continue handler for not found set ok = True;
	set totalDuration = 0;
    open songDurationCursor;
    fetchloop: loop
		fetch songDurationCursor into songDuration;
			if ok = True then 
            leave fetchloop;
            end if;
         set totalDuration = totalDuration + songDuration;
    end loop;
    close songDurationCursor;
    
end $$
delimiter ;