use aptunes;

create user if not exists student identified by 'ikbeneenstudent';
grant execute on procedure GetAlbumDuration to student;
grant execute on procedure GetAlbumDuration2 to student;
grant select on table aptunes.Liedjes to student;