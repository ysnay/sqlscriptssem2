USE Modernways;
CREATE VIEW AuteursBoekenRatings AS
SELECT Auteur, Titel, Rating
FROM auteursboeken INNER JOIN gemiddelderatings ON auteursboeken.boeken_Id = gemiddelderatings.Boeken_Id;