USE Modernways;
ALTER VIEW auteursboeken as
SELECT CONCAT(Personen.Voornaam,'',Personen.Familienaam) AS Auteur,Titel,Boeken.Id AS Boeken_Id
FROM Boeken INNER JOIN publicaties on Boeken.Id = publicaties.Boeken_Id
INNER JOIN Personen on Publicaties.Personen_Id = Personen.Id;