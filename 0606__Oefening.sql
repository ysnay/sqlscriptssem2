SELECT Leden.Voornaam, Boeken.Titel
FROM uitleningen
     INNER JOIN Leden ON uitleningen.Leden_Id = Leden.Id
     INNER JOIN Boeken ON uitleningen.Boeken_Id = Boeken.Id