use Modernways;

alter table Boeken drop column Voornaam, drop column Familienaam;

alter table Boeken add constraint fk_Boeken_Personen
foreign key(Personen_Id) references Personen(Id);
