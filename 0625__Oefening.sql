-- toont per naam het aantal keer dat iemand met die naam lid is van een groep
-- iemand die lid is van twee groepen, wordt dus twee keer geteld
-- naamgenoten zijn mogelijk en worden samen geteld
-- create index FamilienaamVoornaamIdx on Muzikanten(Familienaam,Voornaam); Voor optimaliseren van query v1
-- create index VoornaamFamilienaamIdx on Muzikanten(Voornaam,Familienaam); Voor optimaliseren van query v2
select Voornaam, Familienaam, count(Lidmaatschappen.Muzikanten_Id)
from Muzikanten inner join Lidmaatschappen
on Lidmaatschappen.Muzikanten_Id = Muzikanten.Id
group by Familienaam, Voornaam
order by Voornaam, Familienaam;