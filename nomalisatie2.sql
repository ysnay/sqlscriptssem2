use Modernways;

alter table Personen add(
Id int auto_increment primary key,
Aanspreektitel varchar(30) null,
Straat varchar(80) null,
Huisnummer varchar(5) null,
Stad varchar(50) null,
Commentaar varchar(100) null,
Biografie varchar(400) null
);

select * from Personen order by Familienaam, Voornaam;
