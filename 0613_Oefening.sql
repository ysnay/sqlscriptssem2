USE Modernways;
CREATE VIEW AuteursBoeken AS
SELECT CONCAT(Personen.Voornaam,'',Personen.Familienaam) AS Auteur, Titel
FROM Boeken INNER JOIN Publicaties ON Boeken.Id = Publicaties.Boeken_Id
INNER JOIN Personen ON Publicaties.Personen_ID = Personen.Id;