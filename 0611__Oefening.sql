USE Modernways;
SELECT Games.Titel, coalesce(Releases.Games_Id,'Geen platformen gekend') AS Platformen
FROM games left join releases on Id = Games_Id
WHERE Games_Id is NULL
union
SELECT coalesce(Releases.Platformen_Id,'Geen games gekend'), platformen.Naam
FROM releases RIGHT JOIN Platformen ON Platformen_Id = Id
WHERE Platformen_Id IS NUll;