use ModernWays;

select min(PersoonlijkeGemiddelde) as 'Gemiddelde'
from
(select avg(Cijfer) as 'PersoonlijkeGemiddelde'
from Evaluaties
group by Studenten_Id) as PersoonlijkeGemiddelde;
